CKEditor Google Search -  is an extension to the Drupal 8 CKEditormodule.

Integrates CKEditor's Google Search plugin to Drupal. This enables Drupal's default WYSIWYG text editor capable of google searching the selected words.


REQUIREMENTS
============
- ckeditor


INSTALLATION
============
1- Download the ckeditor_googlesearch folder to your modules directory.
2- Download googlesearch library in the root libraries folder (/libraries).
3- Go to admin/modules and install the module.
4- Go to admin/config/content/formats and configure the desired profile.
5- Move a button into the Active toolbar.
6- Clear your browser's cache, and a new Google Search button will appear in your toolbar.


MODULE DEVELOPERS
=================
Module created by :
Prafull Ranjan ( https://www.drupal.org/u/prafullsranjan )
